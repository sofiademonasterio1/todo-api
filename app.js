const express = require('express'); // Importa el módulo Express para crear la aplicación.
const app = express(); //Crea una nueva instancia de Express.
const port = 3000; //Define el puerto en el que se ejecutará el servidor (3000 en este caso).

//app.use(express.json()); // Habilita el análisis de JSON en las solicitudes entrantes.

const todos = ['bañarme', 'estudiar']; //Inicializa un array vacío para almacenar las tareas 

app.use('/', express.json);

//GET /todos: Devuelve todas las tareas pendientes.
app.get('/', (req, res) => {
    res.send(todos);
  });

//POST /todos: Agrega una nueva tarea pendiente.
app.post('/', (req, res) => {
    const todo = req.body;
    todos.push()
    res.send(todo);
  });

//PUT /todos/:id: Actualiza una tarea pendiente existente con un ID específico.

app.put('/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const updatedTodo = req.body;
  const index = todos.findIndex(todo => todo.id === id);
  
  if (index === -1) {
    res.status(404).send('Todo no encontrado');
  } else {
    todos[index] = updatedTodo;
    res.json(updatedTodo);
  }
});

//DELETE /todos/:id: Elimina una tarea pendiente con un ID específico.
app.delete('/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const index = todos.findIndex(todo => todo.id === id);

  if (index === -1) {
    res.status(404).send('Todo no encontrado');
  } else {
    todos.splice(index, 1);
    res.status(204).send();
  }
});

  

  app.listen(port, () => {
    console.log(`Servidor abierto en http://localhost:${port}`);
  });